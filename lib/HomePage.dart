import 'package:flutter/material.dart';
import 'package:mediaquery1/pages/OrientationExample.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
            MaterialButton(
                color: Colors.deepOrange,
                onPressed: () => {
                  Navigator.push(context, 
                    MaterialPageRoute(
                      builder: (context) => OrientationExample() 
                      )
                   )
                },
                child: Text(
                  "Ir a OrientationExample",
                  style: TextStyle(
                    color: Colors.amberAccent,
                    fontFamily: "Times New Roman",
                    fontWeight: FontWeight.bold
                  ),
                ),
              )
          ]),
        ),
      ),
    );
  }
}
