import 'package:flutter/material.dart';
import 'package:mediaquery1/main.dart';
import 'package:mediaquery1/pages/Landscape.dart';
import 'package:mediaquery1/pages/Portrait.dart';

class OrientationExample extends StatefulWidget {
  OrientationExample({Key? key}) : super(key: key);

  @override
  State<OrientationExample> createState() => _OrientationExampleState();
}

class _OrientationExampleState extends State<OrientationExample> {
  Widget portrait() {
    return Center(
      child: Text('Portrait'),
    );
  }

  Widget landscape() {
    return Center(
      child: Text('Landscape'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("OrientationExample"),
        ),
        body: OrientationBuilder(
          builder: (context, orientation) {
            if (orientation == Orientation.portrait) {
              return Portrait();
            } else {
              return Landscape();
            }
          },
        ));
  }
}
